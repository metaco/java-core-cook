package com.core.spring.bean.entity;

public class SingletonClass {
    private String singletonClassScope;

    public int getScopeValue() {
        return scopeValue;
    }

    public void setScopeValue(int scopeValue) {
        this.scopeValue = scopeValue;
    }

    private int scopeValue;

    public String getSingletonClassScope() {
        return singletonClassScope;
    }

    public void setSingletonClassScope(String singletonClassScope) {
        this.singletonClassScope = singletonClassScope;
    }


}
