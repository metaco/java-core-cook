package com.core.spring.bean.service;

import com.core.spring.bean.entity.Customer;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class CustomerServiceImpCustomMethod implements InitializingBean, DisposableBean {

    private Customer customer;

    public CustomerServiceImpCustomMethod() {
        System.out.println("Call constructor for CustomerServiceImpCustomMethod");
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("destroy(): Bean destruction here");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        //
        System.out.println("afterPropertiesSet(): Bean initialization here");
        System.out.println("afterPropertiesSet property customer :" + this.customer.getName());
    }

    @PostConstruct
    public void postConstruct() {
        System.out.println("postConstruct customer :" + this.customer.getName());
        System.out.println("post-construct():  " +
                "perform some initialization after all the setter methods have been called");
    }

    @PreDestroy
    public void predestroy() {
        System.out.println("pre-destroy()");
    }

    public void customInitBean()  {
        System.out.println("customInitBean()");
    }

    public void customDestroyBean() {
        System.out.println("customDestroyBean()");
    }
}
