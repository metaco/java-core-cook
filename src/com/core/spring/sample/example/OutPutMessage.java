package com.core.spring.sample.example;

public class OutPutMessage {
    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    public void saySomething() {
        System.out.println(message);
    }
}
