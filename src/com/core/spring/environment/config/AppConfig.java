package com.core.spring.environment.config;


import com.core.spring.environment.ApplicationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:app.properties")
public class AppConfig {

    @Autowired
    private Environment environment;

    @Bean
    public ApplicationProperties getAppProperties() {
        ApplicationProperties applicationProperties = new ApplicationProperties();
        applicationProperties.setConnectionUrl(
                environment.getProperty("app.connection.url"));
        applicationProperties.setConnectionName(
                environment.getProperty("app.connection.names"));
        return applicationProperties;
    }
}
