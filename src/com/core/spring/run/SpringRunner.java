package com.core.spring.run;

import com.core.spring.sample.example.OutPutMessage;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringRunner {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        OutPutMessage outPutMessage = (OutPutMessage) context.getBean("outPutMessage");
        outPutMessage.saySomething();
    }
}
