package com.core.spring.run;

import com.core.spring.bean.entity.SingletonClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringBeanScopeApplication {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("beans.xml");

        SingletonClass singletonClass = (SingletonClass) applicationContext.getBean("singletonClass");
        System.out.println(singletonClass.getSingletonClassScope());
        System.out.println(singletonClass.getScopeValue());

        singletonClass.setScopeValue(1);
        // 同一个容器的对象
        SingletonClass singletonClass1 = (SingletonClass) applicationContext.getBean("singletonClass");

        System.out.println(singletonClass1.getScopeValue());
    }
}
