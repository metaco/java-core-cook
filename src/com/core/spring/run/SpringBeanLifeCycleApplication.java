package com.core.spring.run;


import com.core.spring.bean.service.CustomerServiceImpCustomMethod;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringBeanLifeCycleApplication {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("bean_imp_custom_method.xml");

        CustomerServiceImpCustomMethod service = (CustomerServiceImpCustomMethod)
                context.getBean("customerServiceCustomerMethod");

        // spring 生命周期调用栈
        System.out.println("Context has been initialized");
        System.out.println("Already retrieved Bean from context.");
        System.out.println("Customer Name: " + service.getCustomer().getName());
        System.out.println("context.closing ...");
        context.close();

    }
}
