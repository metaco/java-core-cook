package com.core.spring.run;

import com.core.spring.environment.ApplicationProperties;
import com.core.spring.environment.config.AppConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

public class SpringPropertyApplication {
    public static void main(String[] args) {
        // 使用 Annotation Application Context
        AbstractApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(AppConfig.class);

        ApplicationProperties applicationProperties =
                applicationContext.getBean(ApplicationProperties.class);
        System.out.println(applicationProperties.getConnectionName());
        System.out.println(applicationProperties.getConnectionUrl());

        applicationContext.close();
    }

}
