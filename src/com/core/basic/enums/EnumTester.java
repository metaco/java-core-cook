package com.core.basic.enums;

public class EnumTester {
    public static void main(String[] args) {
        // 字符
        for (ColorEnum color : ColorEnum.values()) {
            System.out.println(color);
        }
        System.out.println("----");
        ColorEnum ce = ColorEnum.GRAY;
        // 枚举的引用名
        System.out.println(ce.name()); // GRAY
        // 枚举定义时的序号
        System.out.println(ce.ordinal()); // 1 定义枚举的序号
        // 字符类型和枚举类型转换
        System.out.println(ColorEnum.valueOf("BLUE").name());
        // IllegalArgumentException: No enum constant ColorEnum.FGG
        // ColorEnum.valueOf("FGG");

        System.out.println(EnumStateConstructor.APPROVED.toDescription());
    }
}
