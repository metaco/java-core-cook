package com.core.basic.enums;

public enum EnumStateConstructor {
    // 构造方法是私有，需要杂类内部初始化
    APPROVED("通过"), FORBIDDEN("禁止"), NORMAL("正常");

    private String description;

    EnumStateConstructor(String description) {
        this.description = description;
    }

    public String toDescription() {
        return description;
    }
}
