package com.core.basic.strings;

public class StringBuilderTester {
    public static void main(String[] args) {
        // 字符串缓冲区
        StringBuilder builder = new StringBuilder(1024);

        for (int i = 0;  i < 1000; i ++) {
            builder.append(String.valueOf(i));
        }

        // 链式操作
        builder.append("Name")
                .append("!")
                .insert(0, "Hello , 0");

        System.out.println(builder.toString());
    }
}
