package com.core.basic.annotation;

public class ClassTypeTester {

    @NotNull
    public String name;

    @Ranger(max = 20)
    public int age;

    public ClassTypeTester(@NotNull String name, int age) {
        this.name = name;
        this.age = age;
    }

    public static void main(String[] args) {
        ClassTypeTester typeTester = new ClassTypeTester(null, 25);
    }
}
