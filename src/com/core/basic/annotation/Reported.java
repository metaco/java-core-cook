package com.core.basic.annotation;

// Target 定义Annotation 可以被定义用于源码的哪个位置
// ElementType.TYPE  ， 类，接口
// ElementType.FILED 字段
// 方法 ElementType.METHOD
// 构造方法： ElementType.CONSTRUCTOR
// 方法参数： ElementType.PARAMETER


import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
public @interface Reported {
    int type() default 0;
    String level() default  "level";
    String value() default  "";
}
