package com.core.basic.threads;

public class ThreadTester {
    public static void main(String[] args) {
        System.out.println("main start...");

        Thread t = new Thread() {
            public void run() {
                System.out.println("thread run..");
                System.out.println("thread end...");
            }
        };
        // 必须调用start 方法才能启动新的线程
        t.start();
        System.out.println("main end..");
    }
}
