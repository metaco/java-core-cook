package com.core.basic.iostream.files;

import java.io.File;
import java.io.IOException;

public class FileTester {
    public static void main(String[] args) {
        File fileObject = new File("./");
        System.out.println(fileObject.getAbsolutePath());

        String[] fileStrArr = fileObject.list();
        assert fileStrArr != null;
        for (String item : fileStrArr) {
            System.out.println(item);
        }

        File readme = new File("./src/readme.txt");
        boolean newFile = false;
        try {
            newFile = readme.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(newFile);
    }
}
