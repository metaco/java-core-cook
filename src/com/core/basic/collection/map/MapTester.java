package com.core.basic.collection.map;


import java.util.*;

public class MapTester {
    public static void main(String[] args) {
        Map<String , Integer> entryMap = new HashMap<>();
        entryMap.put("Document", 10);
        entryMap.put("property", 11);

        for (Map.Entry<String, Integer> entry : entryMap.entrySet()) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }

        // 存放时按照 key 的顺序排序
        Map<String , Integer> sortedKeyMap = new TreeMap<>();
        sortedKeyMap.put("Container", 11);
        sortedKeyMap.put("Abstract", 12);

        for (Map.Entry<String, Integer> entry : sortedKeyMap.entrySet()) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }

        Map<ElementMapType, String> elementTypeStringMap = new HashMap<>();
        List<ElementMapType> list = Arrays.asList(
                new ElementMapType("Moment", 10),
                new ElementMapType("Jetbrain", 20),
                new ElementMapType("Data", 12));

        Map<ElementMapType, String> maps = new HashMap<>();
        for (ElementMapType item : list) {
            maps.put(item, item.getTypeName());
        }
        // 作为Key 的对象必须复写 equals, hashCode
        System.out.println(maps.get(new ElementMapType("Moment", 10)));
    }
}
