package com.core.basic.collection.map;

import java.util.Objects;

public class ElementMapType {
    private String typeName;
    private int typeValue;

    public ElementMapType(String typeName, int typeValue) {
        this.typeName = typeName;
        this.typeValue = typeValue;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public int getTypeValue() {
        return typeValue;
    }

    public void setTypeValue(int typeValue) {
        this.typeValue = typeValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ElementMapType)) return false;
        ElementMapType that = (ElementMapType) o;
        return getTypeValue() == that.getTypeValue() &&
                Objects.equals(getTypeName(), that.getTypeName());
    }

    @Override
    public int hashCode() {
        // 和 equals 实现得原理必须一致 保证TypeName, TypeValue 的hash
        return Objects.hash(getTypeName(), getTypeValue());
    }
}
