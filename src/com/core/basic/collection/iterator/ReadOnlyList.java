package com.core.basic.collection.iterator;

import java.util.Iterator;

public class ReadOnlyList<E> implements Iterable<E> {

    private E[] array;

    public ReadOnlyList(E... array) {
        this.array = array;
    }

    @Override
    public Iterator<E> iterator() {
        return new ReadOnlyIterator();
    }

    class ReadOnlyIterator implements Iterator<E> {
        int index = 0;

        @Override
        public boolean hasNext() {
            return index < ReadOnlyList.this.array.length;
        }

        @Override
        public E next() {
            return array[index++];
        }

        @Override
        public void remove() {

        }
    }
}
