package com.core.basic.collection.iterator;

public class IteratorTester {
    public static void main(String[] args) {

        // 任何集合都采用同一种访问模型，调用者对集合内部结构一无所知，
        // 是一种抽象数据模型
        ReadOnlyList<String> list = new ReadOnlyList<>("coreJava", "Man", "Priority");
        for (String item : list) {
            System.out.println(item);
        }

    }
}
