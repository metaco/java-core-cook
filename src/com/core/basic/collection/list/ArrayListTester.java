package com.core.basic.collection.list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ArrayListTester {
    public static void main(String[] args) {
        // List Array 转换
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(6);
        // collection.toArray 返回的是 Object[] // 行为元素添加为空
        Integer[] array = list.toArray(new Integer[5]);
        // [1, 2, 6, null, null]

        // 正确的行为
        Integer[] arrays = list.toArray(new Integer[list.size()]);
        System.out.println(arrays[2]);
        Number[] nb = list.toArray(new Number[3]);

        // ArrayStoreException ...
        // String[] sr = list.toArray(new String[3]);


        System.out.println(nb[0]);

        System.out.println(array);

        // Array -> List 转换
        Integer[] integersArray = {1, 3, 4};
        // 注意此时返回的不是 ArrayList ,而是Arrays.asList 实现得一种 List
        // 此时的 List 是只读的类型
        List<Integer> integerList = Arrays.asList(integersArray);

        // 间接装换
        List<Integer> integerArrayList = new ArrayList<>();
        integerArrayList.addAll(integerList);

        // 直接转换
        List<Integer> integerList1 = new ArrayList<>(Arrays.asList(integersArray));
        Integer sum = integerList1.stream().map(e -> e = e * 2).reduce((acc, r) -> acc + r).get();

        // Stream to List
        List<Integer> streamList = integerList1.stream().map(e -> e = e * 3).collect(Collectors.toList());

        for (Integer e : streamList) {
            System.out.println(e.intValue());
        }

        System.out.println(sum);
    }
}
