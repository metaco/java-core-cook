package com.core.basic.collection.list;

import java.util.ArrayList;
import java.util.List;

public class ArrayListEquals {
    public static void main(String[] args) {

        List<String> sList = new ArrayList<>();
        sList.add("A");
        sList.add("B");

        String c = new String("C");
        String c1 = new String("C");
        sList.add(c);

        System.out.println(c.hashCode());
        System.out.println(c1.hashCode());

        System.out.println(sList.contains(c1)); // 确定 元素 "C" 是否是同一个实例

        System.out.println(sList.indexOf("C"));
        System.out.println(sList.indexOf("V")); //  -1

        List<ElementType> elementTypeList = new ArrayList<>();

        // elementTypeList.add(new ElementMapType("document", 2));
        ElementType et =  new ElementType(null, 5);
        ElementType et1 = new ElementType(null, 5);
        // StringBuilder et2 = new StringBuilder("xrs");
        elementTypeList.add(et);

        System.out.println(et1.hashCode());
        System.out.println(et.hashCode());
        // System.out.println(et2.hashCode());
        boolean contains = elementTypeList.contains(et1);
        System.out.println(contains);
    }
}
