package com.core.basic.collection.list;

import java.util.Objects;

public class ElementType {
    private String typeName;
    private int typeValue;

    public ElementType(String typeName, int typeValue) {
        this.typeName = typeName;
        this.typeValue = typeValue;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public int getTypeValue() {
        return typeValue;
    }

    public void setTypeValue(int typeValue) {
        this.typeValue = typeValue;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj instanceof ElementType) {
            ElementType et = (ElementType) obj;
            return Objects.equals(this.typeName, et.typeName)
                    && this.typeValue == et.typeValue;
        }

        return false;
    }
}
