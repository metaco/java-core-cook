package com.core.basic.collection;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesFetcher {
    // Properties 只支持 ASCII 码
    // 如果要使用中文
    public static void main(String[] args) {
        String configFilePath = "/Users/cagliostro/Stacks/Java/IDEAProjects/CoreJava/config/setting.properties";
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(configFilePath));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        System.out.println(properties.getProperty("url.http.method"));
        System.out.println(properties.getProperty("course.title"));
    }
}
