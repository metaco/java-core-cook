package com.core.basic.collection.set;

import java.util.*;

public class SetTester {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();

        set.add("Document");
        set.add("PortalConfig");
        set.add("orange");
        set.add("PortalConfig");

        set.stream().forEach(System.out::println);

        // 如何去除重复元素

        List<String> list1  = Arrays.asList("pear", "doc", "nodes", "doc");
        Set<String> unique = new HashSet<>(list1);
        List<String> list2 = new ArrayList<>(unique);

        list2.stream().forEach(System.out::println);

        // 按照顺序保留 集合元素

        String[] storageStr = {"abc", "xyz", "abc", "www", "edu", "www", "abc"};
        List<String> strList = Arrays.asList(storageStr);
        Set<String> du = new LinkedHashSet<>(strList);

        //

        List<String> after = new ArrayList<>(du);

        after.stream().forEach(System.out::println);

    }
}
