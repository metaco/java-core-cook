package com.core.basic.collection.queue;

public class ElementTypeItem implements Comparable<ElementTypeItem>{


    private int item;
    private String name;

    public ElementTypeItem(int item, String name) {
        this.item = item;
        this.name = name;
    }

    public int getItem() {
        return item;
    }

    public void setItem(int item) {
        this.item = item;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String  toString() {
        return "ElementTypeItem{" +
                "item=" + item +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int compareTo(ElementTypeItem o) {
        return this.name.compareTo(o.getName());
    }

}
