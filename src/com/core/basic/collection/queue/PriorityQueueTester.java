package com.core.basic.collection.queue;

import java.util.PriorityQueue;
import java.util.Queue;

public class PriorityQueueTester {
    public static void main(String[] args) {
        Queue<ElementTypeItem> queue = new PriorityQueue<>();
        queue.offer(new ElementTypeItem(12, "Ming"));
        queue.offer(new ElementTypeItem(12, "Cing"));
        queue.offer(new ElementTypeItem(14, "Ving"));

        // 使用了优先队列
        System.out.println(queue.poll());

    }
}
