package com.core.basic.collection.queue;

import java.util.LinkedList;
import java.util.Queue;

public class LinkedListQueue {
    public static void main(String[] args) {
        Queue<String> queue = new LinkedList<>();

        // add failed will be throw Exception
        // offer failed will be return false
        if (queue.offer("Hai Queue List")) {
            System.out.println("add ok!");
        } else {
            System.out.println("add failed");
        }

        if (queue.isEmpty()) {
            // can not poll
        }else {
            String first  = queue.peek();
            System.out.println(first);
            System.out.println(queue.size());
            queue.poll();
            System.out.println(queue.size());
        }



    }
}
