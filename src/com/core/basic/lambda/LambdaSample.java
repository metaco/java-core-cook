package com.core.basic.lambda;

import java.util.Arrays;
import java.util.Comparator;

public class LambdaSample {
    public static void main(String[] args) {
        String[] words = {"Improving code with Lambda expression in Java", "document"};

//        Arrays.sort(words, new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                return o1.toLowerCase().compareTo(o2.toLowerCase());
//            }
//        });

//        Arrays.sort(words, (s1, s2) -> {
//            return s1.toLowerCase().compareTo(s2.toLowerCase());
//        });

        Arrays.sort(words, Comparator.comparing(String::toLowerCase));

        System.out.println(Arrays.toString(words));

        Thread ts = new Thread( () -> {
            System.out.println("start new thread");
        });

        ts.start();
    }
}
