package com.core.basic.lambda;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

public class IntStreamRange {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(10);

//        IntStream.range(0, 5).forEach(i -> {
//            executorService.submit(new Runnable() {
//                @Override
//                public void run() {
//                    System.out.println("Running task " + i);
//                }
//            });
//        });

        // 因为 Runnable 是一个函数接口，
        // 所以我们可以轻松地将匿名的内部类替换为拉姆达表达式

        IntStream.range(6, 10)
                .forEach(i -> executorService.submit(() -> System.out.println("Running task " + i)));

        executorService.shutdown();// 关闭线程池
    }
}
