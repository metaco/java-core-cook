package com.core.basic.lambda;

import java.util.Arrays;

public class LambdaTest {
    public static void main(String[] args) {
        String[] plates = new String[] {
            "Mercury", "Venus", "Earth", "Mars", "Jupiter", "Uranus"
        };

        System.out.println(Arrays.toString(plates));
        Arrays.sort(plates);
        System.out.println(Arrays.toString(plates));

        Arrays.sort(plates, (first, second) -> first.length() - second.length());
        System.out.println(Arrays.toString(plates));
    }
}
