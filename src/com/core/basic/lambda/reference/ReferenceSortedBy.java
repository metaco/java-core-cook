package com.core.basic.lambda.reference;

import java.util.Arrays;

public class ReferenceSortedBy {
    public static void main(String[] args) {
        String[] words = {
                "sorted",
                "by",
                "catalina"
        };

        Arrays.sort(words, SortedBy::ignoreCase);

        System.out.println(Arrays.toString(words));
    }
}
