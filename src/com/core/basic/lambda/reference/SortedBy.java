package com.core.basic.lambda.reference;

public class SortedBy {
    /**
     *
     * @param s1
     * @param s2
     * @return
     */
    static int ignoreCase(String s1, String s2) {
        return s1.toLowerCase().compareTo(s2.toLowerCase());
    }
}
