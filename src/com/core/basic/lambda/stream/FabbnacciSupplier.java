package com.core.basic.lambda.stream;

import java.math.BigInteger;
import java.util.function.Supplier;

public class FabbnacciSupplier implements Supplier<BigInteger> {
    private BigInteger next = BigInteger.ONE;
    private BigInteger current = BigInteger.ONE;

    public static int step = 0;
    @Override
    public BigInteger get() {
        System.out.println("get......" + step);
        if (step == 1 || step == 2) {
            ++step;
            System.out.println("step : " + step);
            return current;
        }
        next = current.add(next);
        current = next;
        return next;
    }
}
