package com.core.basic.lambda.stream;

import java.util.Arrays;
import java.util.stream.Stream;

public class StreamMapper {
    public static void main(String[] args) {
        // map 映射操作
        // T 类型转换为 R 类型
        String[] arrayWord = {"Stream Api supports functional-style"};

        Stream<String> stream = Arrays.stream(arrayWord);
        stream.map(v -> {
           return v.toUpperCase();
        }).forEach(System.out::println);

        // filter
        Stream<Integer> s = Stream.of(1, 3, 5, 6, 7);
        Stream<Integer> s2 = s.filter((n) -> n % 2 == 1);
        System.out.println(Arrays.toString(s2.toArray()));
        // {1, 3, 5, 7}

        // reduce
        // reduce 无初始值:
        System.out.println(Stream.of(1, 2, 4, 9).reduce((acc, n) -> acc + n));
        // 16

        // reduce 初始值
        System.out.println(Stream.of(1, 3, 4, 10).reduce(100, (pre, next) -> pre + next));
        // 118

        Integer reduceSum = Stream.of(1, 3, 4, 10).reduce(100, (pre, next) -> pre + next);
        System.out.println(reduceSum);
        // 118

        // reduce - join
        String[] strArr = {"supports", "functional", "operations"};
        Stream<String> strStream = Arrays.stream(strArr);
        String b = strStream.map(String::toUpperCase).reduce((acc, p) -> acc + " ... " + p).get();
        System.out.println(b);
        // SUPPORTS ... FUNCTIONAL ... OPERATIONS
        // sorted
        String[] testStr = {"HELLO", null, " ", "woRLd ", " java"};
        Stream<String> testStream = Arrays.stream(testStr);
        Stream<String> afterFiltered = testStream.filter(i -> i != null && !i.trim().isEmpty())
                .map(q -> q.trim()).map(i -> {
                    return i.substring(0, 1).toUpperCase() + i.toLowerCase().substring(1);
                });
        System.out.println(Arrays.toString(afterFiltered.sorted().toArray()));
        // {Hello, Java, World}

        // Java Collection to stream to a new collection
        /**
         * Get an arbitrary List implementation holding the result:
         *
         * List<T> results = l.stream().filter(…).collect(Collectors.toList());
         * Get an arbitrary Set implementation holding the result:
         *
         * Set<T> results = l.stream().filter(…).collect(Collectors.toSet());
         * Get a specific Collection:
         *
         * ArrayList<T> results =
         *   l.stream().filter(…).collect(Collectors.toCollection(ArrayList::new));
         */
    }
}
