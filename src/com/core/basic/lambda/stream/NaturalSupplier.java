package com.core.basic.lambda.stream;

import java.math.BigInteger;
import java.util.function.Supplier;

/**
 *  获取大自然整数
 */
public class NaturalSupplier implements Supplier<BigInteger> {
    private BigInteger next = BigInteger.ZERO;
    @Override
    public BigInteger get() {
        next = next.add(BigInteger.ONE);
        return next;
    }
}
