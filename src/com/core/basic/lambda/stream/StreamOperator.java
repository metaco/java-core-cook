package com.core.basic.lambda.stream;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.stream.Stream;

public class StreamOperator {
    // 存储：顺序输出的任意Java 对象
    // 用途：内存计算，业务逻辑

    // I/O stream： 存储的顺序读写的 byte / char 字符
    // 用途： 序列化文件至网络

    // List 内存当中已经存在的对象元素
    // Stream 操作的是未分配（一段规则），实时计算 ,只在最后的计算时实时计算

    // 产生 Stream 操作流
    public static void main(String[] args) {
        Stream<Integer> stream = Stream.of(1, 4, 5, 5);

        Stream<BigInteger> bs = Stream.generate(new NaturalSupplier());
//        bs.limit(100).forEach(v -> {
//            System.out.println(v);
//        });
        // 无限序列变成有限序列
        bs.limit(20).forEach(System.out::println);

        String[] words = {"Java API", "SDK", "we are the world"};
        long wordCount = Arrays.stream(words)
                    .filter(s -> s.equals(s.toLowerCase()) )
                    .count();

        Stream<BigInteger> bsF = Stream.generate(new FabbnacciSupplier());
        System.out.println("-------------------");
        // 无限序列变成有限序列
        bsF.limit(10).forEach(System.out::println);



        System.out.println(wordCount);
    }


}
