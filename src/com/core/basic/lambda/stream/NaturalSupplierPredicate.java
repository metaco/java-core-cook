package com.core.basic.lambda.stream;

import java.math.BigInteger;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class NaturalSupplierPredicate implements Supplier<BigInteger> , Predicate<BigInteger> {
    private BigInteger next = BigInteger.ZERO;
    @Override
    public BigInteger get() {
        next = next.add(BigInteger.ONE);
        return next;
    }

    @Override
    public boolean test(BigInteger bigInteger) {
        return false;
    }
}
