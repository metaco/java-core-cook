package com.core.basic.designpattern.state;

public interface Role {
    void doWork();
}
