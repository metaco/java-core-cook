package com.core.basic.designpattern.state;

import java.util.Arrays;
import java.util.LinkedList;

public class InheritanceToCompose {
    // 变继承关系为组合关系， state 模式，Role Object 模式
    public static void main(String[] args) {
        Employee employee1 = new Employee("John", 10000, new Engineer());
        Employee employee2 = new Employee("Mary", 10000, new Engineer());

        LinkedList<Employee> employees = new LinkedList<>();
        employees.add(employee1);
        employees.add(employee2);

        for (Employee employee : employees) {
            System.out.println(employee);
        }

        // 动态改变一个角色
        employee2.setRole(new Manager(Arrays.asList(employee1)));
        for (Employee employee: employees) {
            System.out.println(employee);
        }

        employee1.doWork();
        employee2.doWork();
    }
}
