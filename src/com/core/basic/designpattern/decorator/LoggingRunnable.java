package com.core.basic.designpattern.decorator;

public class LoggingRunnable implements Runnable {

    private final Runnable innerRunnable;

    public LoggingRunnable(Runnable innerRunnable) {
        this.innerRunnable = innerRunnable;
    }

    @Override
    public void run() {
        System.out.println("task started at " + System.currentTimeMillis());
        innerRunnable.run();
        System.out.println("task finished at " + System.currentTimeMillis());
    }
}
