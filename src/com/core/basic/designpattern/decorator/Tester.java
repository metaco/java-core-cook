package com.core.basic.designpattern.decorator;

public class Tester {
    public static void main(String[] args) {
        // new CodingTask().run();

        // new LoggingRunnable(new CodingTask()).run();

        // decorator 模式
        new LoggingRunnable(
                new TransactionRunnable(new CodingTask())
        ).run();
    }
}
