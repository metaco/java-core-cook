package com.core.basic.designpattern.decorator;

public class CodingTask implements Runnable {
    @Override
    public void run() {
        System.out.println("Writing code..");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
