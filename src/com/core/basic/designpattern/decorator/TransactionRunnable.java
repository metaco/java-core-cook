package com.core.basic.designpattern.decorator;

public class TransactionRunnable implements Runnable{

    private final Runnable innerRunnable;

    public TransactionRunnable(Runnable innerRunnable) {
        this.innerRunnable = innerRunnable;
    }

    @Override
    public void run() {
        boolean shouldRollback = false;
        try {
            beginTransaction();
            innerRunnable.run();
        } catch (Exception e) {
            shouldRollback = true;
            // 这里的异常不可以（吃掉异常) 处理掉，应该是抛出去
            throw e;
        } finally {
            if (shouldRollback) {
                rollback();
            } else {
                commit();
            }
        }
    }

    private void beginTransaction() {
        System.out.println("begin transaction..");
    }

    private void rollback() {
        System.out.println("rollback..");
    }

    private void commit() {
        System.out.println("commit..");
    }
}
