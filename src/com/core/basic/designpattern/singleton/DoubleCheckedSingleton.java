package com.core.basic.designpattern.singleton;

public class DoubleCheckedSingleton {

    private static volatile DoubleCheckedSingleton instance;

    private DoubleCheckedSingleton() {
    }

    public static DoubleCheckedSingleton getInstance() {
        if (null == instance) {
            synchronized (DoubleCheckedSingleton.class) {
                if (null == instance) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    instance = new DoubleCheckedSingleton();
                }
            }
        }
        return instance;
    }

    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            DoubleCheckedSingleton ds = DoubleCheckedSingleton.getInstance();
            System.out.println(ds.hashCode());
        });
        Thread t2 = new Thread(() -> {
            DoubleCheckedSingleton ds = DoubleCheckedSingleton.getInstance();
            System.out.println(ds.hashCode());
        });
         t1.start();
         t2.start();

    }
}
