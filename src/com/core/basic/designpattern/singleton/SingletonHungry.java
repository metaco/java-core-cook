package com.core.basic.designpattern.singleton;

/**
 * 饿汉式：
 * 类加载到内存后，实例化一个单例， JVM线程安全简单实用，
 * 缺点： 不管用到与否，类装载时就完成实例化
 */
public class SingletonHungry {
    private static final SingletonHungry instance  = new SingletonHungry();

    private SingletonHungry() { }

    public static SingletonHungry getInstance() {
        return instance;
    }

    public static void main(String[] args) {
        SingletonHungry hungry1 = SingletonHungry.getInstance();
        SingletonHungry hungry2 = SingletonHungry.getInstance();

        System.out.println(hungry1.hashCode());
        System.out.println(hungry2.hashCode());
    }
}
