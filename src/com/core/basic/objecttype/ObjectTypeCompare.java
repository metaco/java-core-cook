package com.core.basic.objecttype;

public class ObjectTypeCompare {
    public static void main(String[] args) {
        //
        Integer a = 2;
        Integer b = new Integer(2);
        System.out.println(a.equals(b)); // true
        System.out.println(a == b);  // false
        System.out.println(Integer.valueOf(1000) ==  Integer.valueOf(1000));
        // true or false 这里取决于 实现时的数据 low, high

        // 这里发生了 拆箱操作
        System.out.println("new Integer(2) == 2 ? " + (new Integer(2) == 2)); // true

        System.out.println("Integer.valueOf(2).intValue() == 2 ? "
                + (Integer.valueOf(2).intValue() == 2)); // true
    }
}
